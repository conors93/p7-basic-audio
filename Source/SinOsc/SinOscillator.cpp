//
//  SinOscillator.cpp
//  JuceBasicAudio
//
//  Created by conor sweetingham on 07/11/2017.
//
//

#include "SinOscillator.hpp"
#include <cmath>

SinOscillator::SinOscillator()
{
    reset();
}

SinOscillator::~SinOscillator()
{
    
}

void SinOscillator::setFrequency (float freq)
{
    frequency = freq;
    phaseInc = (2 * M_PI * frequency ) / sampleRate ;
}

void SinOscillator::setNote (int noteNum)
{
    setFrequency (440.f * pow (2, (noteNum - 69) / 12.0));
}

void SinOscillator::setAmplitude (float amp)
{
    amplitude = amp;
}

void SinOscillator::reset()
{
    phase = 0.f;
    sampleRate = 44100;
    setFrequency (440.f);
    setAmplitude (0.f);
}

void SinOscillator::setSampleRate (float sr)
{
    sampleRate = sr;
    setFrequency (frequency);//just to update the phaseInc
}

float SinOscillator::nextSample()
{
    float out = renderWaveShape (phase) * amplitude ;
    phase = phase + phaseInc ;
    if(phase  > (2.f * M_PI))
        phase -= (2.f * M_PI);
    
    return out;
}

float SinOscillator::renderWaveShape (const float currentPhase)
{
    return sin (currentPhase);
}